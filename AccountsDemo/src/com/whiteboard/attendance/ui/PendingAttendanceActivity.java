package com.whiteboard.attendance.ui;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.adapters.PendingAttendanceListAdapter;
import com.whiteboard.attendance.helper.DataHelper;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.helper.MessageHelper;
import com.whiteboard.attendance.util.Attendance;
import com.whiteboard.attendance.util.Student;
import com.whiteboard.attendance.util.TimeSlot;

public class PendingAttendanceActivity extends AppCompatActivity {
	Context c;
	ArrayList<Attendance> pendingAttendanceList;
	DataHelper helper;
	SQLiteDatabase db;
	Cursor cur;
	TextView tv;
	Button newAttendance;
	Attendance attendance;
	MessageHelper msg;
	PendingAttendanceListAdapter adapter;
	ListView lv;
	String className,subjectName, topicName,type;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		c = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pending_attendance);
		tv=(TextView)findViewById(R.id.noAttendance);
		newAttendance=(Button) findViewById(R.id.addMore);
		newAttendance.setBackgroundResource(R.drawable.add);
		lv=(ListView)findViewById(R.id.pendingList);
		helper = new DataHelper(this);
		pendingAttendanceList=new ArrayList<Attendance>();
		// creates database if not created
		db = helper.getReadableDatabase();
		// getting db to read
		cur = db.rawQuery("SELECT * FROM pendingattendance", null);
		msg=new MessageHelper(c);
		Cursor cur1=db.rawQuery("SELECT * FROM pendingattendance", null);;
		// execute querys
		if (cur != null) {
			// first entry is always the dummy entry
			if (!cur.moveToNext()) {
				// first entry is the only entry
				tv.setText("No Pending Attendance");
			} else {
				// there are more entries
				tv.setVisibility(View.GONE);
				while (cur1.moveToNext()) {
					Integer attendance_id=cur1.getInt(0);
					Integer class_id = cur1.getInt(1);// gets class_id
					Integer subject_id = cur1.getInt(2);// gets subject_id
					Integer topic_id = cur1.getInt(3);// gets topic_id
					Integer slot_id = cur1.getInt(4);// gets slot_id
					Integer type_id=cur1.getInt(5);//gets type+id
					String date=cur1.getString(6);// gets date
					String timestamp=cur1.getString(7);//gets timestamp
					Log.e("CLASS", class_id+"");
					Cursor cur2=db.rawQuery("Select * from classes where id="+class_id+";", null);
					if(cur2!=null){
						while(cur2.moveToNext()){
							className=cur2.getString(1);
						}
					}
					cur2=db.rawQuery("Select * from subjects where id="+subject_id+";", null);
					if(cur2!=null){
						while(cur2.moveToNext()){
							subjectName=cur2.getString(1);
						}
					}
					cur2=db.rawQuery("Select * from topics where id="+topic_id+";", null);
					if(cur2!=null){
						while(cur2.moveToNext()){
							topicName=cur2.getString(1);
						}
					}
					
					cur2=db.rawQuery("Select * from slots where id="+slot_id+";", null);
					TimeSlot ts=new TimeSlot();
					if(cur2!=null){
						while(cur2.moveToNext()){
							ts.setStart_time(cur2.getString(1));
							ts.setEnd_time(cur2.getString(2));
							ts.setSlot_id(cur2.getString(0));	
						}
					}
					cur2=db.rawQuery("Select * from attendancetype where id="+type_id+";", null);
					if(cur2!=null){
						while(cur2.moveToNext()){
							 type=cur2.getString(1);
						}
					}
					attendance=new Attendance(attendance_id,className, subjectName, topicName, date, ts,type,timestamp,subject_id,topic_id,class_id);
					pendingAttendanceList.add(attendance);
					adapter=new PendingAttendanceListAdapter(c, pendingAttendanceList);
					lv.setAdapter(adapter);
				}
			}
		}
		else{
			msg.displayShort("cur==null");
		}
		newAttendance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(PendingAttendanceActivity.this,NewAttendanceActivity.class);
				finish();
				PendingAttendanceActivity.this.startActivity(i);
			
				
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					int position, long id) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builderSingle = new AlertDialog.Builder(
						PendingAttendanceActivity.this);
				builderSingle.setIcon(R.drawable.ic_launcher);
				builderSingle.setTitle("Your Option-");
				final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
						c,
						android.R.layout.select_dialog_item);
				arrayAdapter.add("Delete");
				arrayAdapter.add("Edit");
				//arrayAdapter.add("Edit Attendance");
				//arrayAdapter.add("Edit Slot");
				//arrayAdapter.add("Edit Type");
				final Attendance a=pendingAttendanceList.get(position);
				builderSingle.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				builderSingle.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								String strName = arrayAdapter
										.getItem(which);
								switch (strName) {
									case "Delete":
										AlertDialog.Builder builder2 = new AlertDialog.Builder(
												c);
										builder2.setTitle("Delete");
										builder2.setMessage("Do you want to delete selected Attendance"
										+"( "+a.getClassName()+", "+a.getSubjectName()+", "+
												a.getSlot().getStart_time()+"-"+a.getSlot().getEnd_time()
												+" ) ?"
												);
										builder2.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.dismiss();
													}
												});
										builder2.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														// TODO Auto-generated method stub
														Integer id=a.getAttendance_id();
														db=helper.getWritableDatabase();
														db.execSQL("Delete FROM attendance where pendingattendance_id="+id+";");
														db.execSQL("Delete FROM pendingattendance where id="+id+";");
														Intent i=PendingAttendanceActivity.this.getIntent();
														finish();
														startActivity(i);
														msg.displayShort("Successful Deletion");
													}
												
												});
										builder2.show();
										break;
									case "Edit":
										builder2 = new AlertDialog.Builder(
												c);
										builder2.setTitle("Delete");
										builder2.setMessage("Do you want to edit selected Attendance"
										+"( "+a.getClassName()+", "+a.getSubjectName()+", "+
												a.getSlot().getStart_time()+"-"+a.getSlot().getEnd_time()
												+" ) ?"
												);
										builder2.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.dismiss();
													}
												});
										builder2.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														// TODO Auto-generated method stub
														Integer id=a.getAttendance_id();
														ArrayList<Student> presentNoList=new ArrayList<Student>();
														ArrayList<Student> absentNoList=new ArrayList<Student>();
														ArrayList<Student> rollNoList=new ArrayList<Student>();	
														db=helper.getReadableDatabase();
														cur=db.rawQuery("Select * from pendingattendance where id="+id+";", null);
														Integer subject_id=0;
														Integer class_id=0;
														Integer topic_id=0;
														Integer type_id=0;
														Integer slot_id=0;
														if(cur!=null){
															while(cur.moveToNext()){
																class_id=cur.getInt(1);
																subject_id=cur.getInt(2);
																topic_id=cur.getInt(3);
																slot_id=cur.getInt(4);
																type_id=cur.getInt(5);
															}
														}
														cur = db.rawQuery("Select * from subjects where id="
																+ subject_id + ";", null);
														if (cur != null) {
															while (cur.moveToNext()) {
																String isElective = cur.getString(3);
																if (isElective.equalsIgnoreCase("0")) {
																	Cursor cur1=db.rawQuery("Select * from class_rollno where class_id="+class_id+";", null);
																	if(cur1!=null){
																		rollNoList.clear();
																		while(cur1.moveToNext()){
																			String roll_no=cur1.getString(1);
																			Integer student_id=cur1.getInt(2);
																			Student s=new Student();
																			s.setId(student_id+"");
																			s.setRoll_no(roll_no);
																			rollNoList.add(s);
																		}
																	}
																} else {
																	Cursor cur1 = db.rawQuery(
																			"Select * from subject_rollno where subject_id="
																					+ subject_id + ";", null);
																	if (cur1 != null) {
																		rollNoList.clear();
																		while (cur1.moveToNext()) {
																			String roll_no=cur1.getString(1);
																			Integer student_id=cur1.getInt(2);
																			Student s=new Student();
																			s.setId(student_id+"");
																			s.setRoll_no(roll_no);
																			rollNoList.add(s);
																		}
																	}
																}
															}
														} else {

														}

														cur=db.rawQuery("SELECT * FROM attendance where pendingattendance_id="+id+";", null);
														if(cur!=null){
															while(cur.moveToNext()){
																Integer roll_no=cur.getInt(1);
																String present=cur.getString(2);
																Integer student_id=cur.getInt(3);
																Student s=new Student();
																s.setId(student_id+"");
																s.setRoll_no(roll_no+"");
																if(present.equalsIgnoreCase("true")){
																	presentNoList.add(s);
																}
																else if(present.equalsIgnoreCase("false")){
																	absentNoList.add(s);
																}	
															}
														}
														Intent i=new Intent(PendingAttendanceActivity.this,EditAttendanceActivity.class);
														i.putExtra("absentNoList", absentNoList);
														i.putExtra("presentNoList", presentNoList);
														i.putExtra("rollNoList", rollNoList);
														i.putExtra("id", id);
														i.putExtra("class_id", class_id);
														i.putExtra("subject_id", subject_id);
														i.putExtra("topic_id", topic_id);
														i.putExtra("slot_id", slot_id);
														i.putExtra("type_id", type_id);
														
														finish();
														startActivity(i);
														//msg.displayShort("Successful Deletion");
													}												
												});
										builder2.show();
										break;
										
								}
							}
				});
				builderSingle.show();;
				
				return true;
			}
		});
			
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		MenuItems items = new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
