package com.whiteboard.attendance.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.util.Values;

public class ClientDetailsActivity extends AppCompatActivity {
	TextView clientId,clientSecret;
	Button submit;
	Context c;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_client_details);
		c=this;
		clientId=(TextView)findViewById(R.id.clientId);
		clientSecret=(TextView)findViewById(R.id.clientSecret);
		submit=(Button)findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String clientIdValue=clientId.getText().toString();
				String clientSecretValue=clientSecret.getText().toString();
				if(clientIdValue.equals("")||clientSecretValue.equals("")){
					Toast.makeText(c, "Insert appropriate values", Toast.LENGTH_SHORT).show();
				}
				else{
					SharedPreferences.Editor editor = getSharedPreferences(
							Values.USER_INFO, 0).edit();
					editor.putString(Values.CLIENT_ID,
							clientIdValue);
					editor.putString(Values.CLIENT_SECRET,clientSecretValue);
					Values.client_id=clientIdValue;
					Values.client_secret=clientSecretValue;
					Toast.makeText(c, "Client Id="+clientIdValue+"Client Secret="+clientSecretValue, Toast.LENGTH_SHORT).show();
					Log.e("CLIENT VALUES:", "Client Id="+clientIdValue+"Client Secret="+clientSecretValue);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		MenuItems items = new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
