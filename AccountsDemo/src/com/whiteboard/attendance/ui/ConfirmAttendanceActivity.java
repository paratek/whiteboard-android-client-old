package com.whiteboard.attendance.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.adapters.GridAdapter;
import com.whiteboard.attendance.helper.DataHelper;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.helper.MessageHelper;
import com.whiteboard.attendance.util.Student;
import com.whiteboard.attendance.util.Values;

public class ConfirmAttendanceActivity extends AppCompatActivity {
	Context c;
	//Switch markPresent;
	Integer presentCount;
	String presentDetails;
	Button confirmAttendance;
	Bundle retrieve;
	Integer classCapacity;
	Integer class_id, subject_id, topic_id, slot_id, type_id;
	String method;
	GridView gv;
	TextView presentTextView;
	ArrayList<Student> rollNoList = new ArrayList<Student>();
	Boolean markAbsent;
	GridAdapter adapter;
	MessageHelper m;
	ProgressDialog pdialog;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_attendance);
		c = ConfirmAttendanceActivity.this;
		m = new MessageHelper(c);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View customView = mInflater.inflate(R.layout.actionbar_custom, null);
		//markPresent = (Switch) customView.findViewById(R.id.switchForActionBar);
		presentTextView = (TextView) customView
				.findViewById(R.id.presentDetails);
		//markPresent.setText("Mark Absent");
		actionBar.setCustomView(customView);
		actionBar.setDisplayShowCustomEnabled(true);
		gv = (GridView) findViewById(R.id.gridView1);
		confirmAttendance = (Button) findViewById(R.id.confirmAttendance);
		//markPresent.setChecked(true);
		retrieve = getIntent().getExtras();
		class_id = retrieve.getInt("class_id");
		subject_id = retrieve.getInt("subject_id");
		topic_id = retrieve.getInt("topic_id");
		slot_id = retrieve.getInt("slot_id");
		type_id = retrieve.getInt("type_id");
		method=retrieve.getString("method");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(method.equals("Mark Absent")){
					markAbsent=true;
				}
				else
					markAbsent=false;
				rollNoList = (ArrayList<Student>) retrieve.get("rollNoList");
				classCapacity = rollNoList.size();
				if (markAbsent){
					Iterator<Student> i=rollNoList.iterator();
					while(i.hasNext()){
						Student s=i.next();
						Student s1=new Student();
						s1.setId(s.getId());
						s1.setRoll_no(s.getRoll_no());
						Values.presentNoList.add(s1);
					}
					
				}else{
					Iterator<Student> i=rollNoList.iterator();
					while(i.hasNext()){
						Student s=i.next();
						Student s1=new Student();
						s1.setId(s.getId());
						s1.setRoll_no(s.getRoll_no());
						Values.absentNoList.add(s1);
					}
				}
				ConfirmAttendanceActivity.this.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter = new GridAdapter(c, rollNoList, markAbsent);
						gv.setAdapter(adapter);
						confirmAttendance.setOnClickListener(confirmListener);
						presentTextView.setText("Present: "+Values.presentNoList.size()+" / "+rollNoList.size());
					}
				});
				//markPresent.setOnCheckedChangeListener(markPresentListener);
				
			}
		}).start();
		
		
	}

	OnClickListener confirmListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			AlertDialog.Builder builder2 = new AlertDialog.Builder(c);
			builder2.setTitle("Confirm Attendance");
			builder2.setMessage("Do you want to confirm " + " this attendance?");
			builder2.setNegativeButton("No",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			builder2.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							ConfirmAttendanceActivity.this.runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									pdialog=new ProgressDialog(c);
									pdialog.setTitle("Confirming");
									pdialog.setProgressStyle(0);
									pdialog.show();
									pdialog.setCancelable(false);
								}
							});
							
							DataHelper helper = new DataHelper(c);
							SQLiteDatabase db = helper.getWritableDatabase();
							SQLiteDatabase db1 = helper.getReadableDatabase();
							Calendar c1 = Calendar.getInstance();
							SimpleDateFormat timestampFormat = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"yyyy-MM-dd");
							String timestamp = timestampFormat.format(c1
									.getTime());
							String date = dateFormat.format(c1.getTime());
							ContentValues contentValues = new ContentValues();
							contentValues.put("class_id", class_id);
							contentValues.put("subject_id", subject_id);
							contentValues.put("topic_id", topic_id);
							contentValues.put("slot_id", slot_id);
							contentValues.put("type_id", type_id);
							contentValues.put("date", date);
							contentValues.put("timestamp", timestamp);
							// put new entry in pendingattendance
							db.insert("pendingattendance", null, contentValues);
							// Retrieve id of new entry
							Cursor cur = db1.rawQuery(
									"Select id from pendingattendance where timestamp='"
											+ timestamp + "'", null);
							Integer id = null;
							if (cur != null) {
								while (cur.moveToNext()) {
									id = cur.getInt(0);
								}
							}
							// store values in attendance table for current
							// submission
							db = helper.getWritableDatabase();
							contentValues = new ContentValues();
							Iterator<Student> i=Values.presentNoList.iterator();
							while(i.hasNext()){
								Student s=i.next();
								contentValues.put("pendingattendance_id", id);
								contentValues.put("rollno", s.getRoll_no());
								contentValues.put("student_id", s.getId());
								contentValues.put("present", "true");
								db.insert("attendance", null, contentValues);
								contentValues.clear();
							}
							i=Values.absentNoList.iterator();
							while(i.hasNext()){
								Student s=i.next();
								contentValues.put("pendingattendance_id", id);
								contentValues.put("rollno", s.getRoll_no());
								contentValues.put("student_id", s.getId());
								contentValues.put("present", "false");
								db.insert("attendance", null, contentValues);
								contentValues.clear();
							}
							db.close();
							db = helper.getReadableDatabase();
							cur = db.rawQuery(
									"Select * from pendingattendance", null);
							// String ids="";
							if (cur != null) {
								while (cur.moveToNext()) {
									Log.e("pendingattendance",
											cur.getString(0) + "  "
													+ cur.getString(1) + "  "
													+ cur.getString(2) + "  "
													+ cur.getString(3) + " "
													+ cur.getString(4) + "  "
													+ cur.getString(5) + "  "
													+ cur.getString(6) + "  "
													+ cur.getString(7));
								}
							}
							cur = db.rawQuery("Select * from attendance", null);
							// String ids="";
							if (cur != null) {
								while (cur.moveToNext()) {
									Log.e("attendance",
											cur.getString(0) + "  "
													+ cur.getString(1) + "  "
													+ cur.getString(2));
								}
							}
							// Re initialize the values after submission
							Values.presentNoList = new ArrayList<Student>();
							Values.absentNoList = new ArrayList<Student>();
							ConfirmAttendanceActivity.this
									.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											m.displayShort("Attendance Confirmed Successfully");
											Intent i = new Intent(
													ConfirmAttendanceActivity.this,
													PendingAttendanceActivity.class);
											startActivity(i);
											finish();
											pdialog.dismiss();
										}
									});

						}
					});
			builder2.show();

		}
	};
	OnCheckedChangeListener markPresentListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			if (markAbsent) {
				markAbsent = false;
				//markPresent.setText("Mark Present");
				//Values.presentNoList = rollNoList;
				//Values.absentNoList.clear();

			} else {
				markAbsent = true;
				//markPresent.setText("Mark Absent");
				//Values.absentNoList = rollNoList;
				//Values.presentNoList.clear();

			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		MenuItems items = new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
