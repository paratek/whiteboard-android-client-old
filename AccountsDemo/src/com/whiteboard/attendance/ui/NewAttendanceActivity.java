package com.whiteboard.attendance.ui;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.helper.DataHelper;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.helper.MessageHelper;
import com.whiteboard.attendance.util.Student;

public class NewAttendanceActivity extends AppCompatActivity {
	Context c;
	Button confirmDetails;
	Spinner className, subjectName, topicName, slotDetails, attendanceType, attendanceMethod;
	Integer classCapacity;
	Integer class_id, subject_id, topic_id, slot_id, type_id;
	String method;
	MessageHelper msg;
	DataHelper helper;
	SQLiteDatabase db;
	Cursor cur;
	ArrayList<String> classList = new ArrayList<String>();
	ArrayList<String> subjectList = new ArrayList<String>();
	ArrayList<String> topicList = new ArrayList<String>();
	ArrayList<String> slotList = new ArrayList<String>();
	ArrayList<String> typeList = new ArrayList<String>();
	ArrayList<Student> rollNoList = new ArrayList<Student>();
	ArrayList<String> methodList = new ArrayList<String>();
	ArrayAdapter<String> classAdapter;
	ArrayAdapter<String> subjectAdapter;
	ArrayAdapter<String> topicAdapter;
	ArrayAdapter<String> slotAdapter;
	ArrayAdapter<String> typeAdapter;
	ArrayAdapter<String> methodAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		c = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_attendance);
		msg = new MessageHelper(c);
		className = (Spinner) findViewById(R.id.className);
		subjectName = (Spinner) findViewById(R.id.subjectName);
		topicName = (Spinner) findViewById(R.id.topicName);
		slotDetails = (Spinner) findViewById(R.id.slotDetails);
		attendanceType = (Spinner) findViewById(R.id.attendanceType);
		attendanceMethod=(Spinner) findViewById(R.id.attendanceMethod);
		confirmDetails = (Button) findViewById(R.id.confirmDetails);
		confirmDetails.setVisibility(View.GONE);
		subjectName.setVisibility(View.GONE);
		topicName.setVisibility(View.GONE);
		slotDetails.setVisibility(View.GONE);
		attendanceType.setVisibility(View.GONE);
		attendanceMethod.setVisibility(View.GONE);
		helper = new DataHelper(c);
		db = helper.getReadableDatabase();
		cur = db.rawQuery("Select * from classes;", null);
		classList.add("Select Class");
		subjectList.add("Select Subject");
		topicList.add("Select Topic");
		slotList.add("Select Slot");
		typeList.add("Select Type");
		methodList.add("Select Marking Method");
		methodList.add("Mark Absent");
		methodList.add("Mark Present");
		class_id = subject_id = topic_id = type_id = slot_id = -1;
		if (cur != null) {
			while (cur.moveToNext()) {
				classList.add(cur.getString(1));
				Log.e("CLassVALUE", cur.getString(1));
			}
		}
		
		classAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, classList);
		subjectAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, subjectList);
		topicAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, topicList);
		slotAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, slotList);
		typeAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, typeList);
		methodAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, methodList);
		// dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		className.setAdapter(classAdapter);
		topicName.setAdapter(topicAdapter);
		subjectName.setAdapter(subjectAdapter);
		slotDetails.setAdapter(slotAdapter);
		attendanceType.setAdapter(typeAdapter);
		attendanceMethod.setAdapter(methodAdapter);
		
		classAdapter.notifyDataSetChanged();
		className.setOnItemSelectedListener(classListener);
		confirmDetails.setOnClickListener(confirmListener);
		subjectName.setOnItemSelectedListener(subjectListener);
		topicName.setOnItemSelectedListener(topicListener);
		slotDetails.setOnItemSelectedListener(slotListener);
		attendanceType.setOnItemSelectedListener(typeListener);
		attendanceMethod.setOnItemSelectedListener(methodListener);
		className.setSelection(0);
	}

	OnClickListener confirmListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (class_id == -1 || subject_id == -1 || topic_id == -1
					|| type_id == -1 || slot_id == -1 || method.equals("Select Marking Method")) {
				msg.displayLong("Please Select appropriate values");
			} else {

				Intent i = new Intent(NewAttendanceActivity.this,
						ConfirmAttendanceActivity.class);
				i.putExtra("class_id", class_id);
				i.putExtra("subject_id", subject_id);
				i.putExtra("topic_id", topic_id);
				i.putExtra("slot_id", slot_id);
				i.putExtra("type_id", type_id);
				//i.putExtra("classCapacity", classCapacity);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from subjects where id="
						+ subject_id + ";", null);
				if (cur != null) {
					while (cur.moveToNext()) {
						String isElective = cur.getString(3);
						if (isElective.equalsIgnoreCase("0")) {
							Integer class_id=cur.getInt(2);
							Cursor cur1=db.rawQuery("Select * from class_rollno where class_id="+class_id+";", null);
							if(cur1!=null){
								rollNoList.clear();
								while(cur1.moveToNext()){
									String roll_no=cur1.getString(1);
									Integer student_id=cur1.getInt(2);
									Student s=new Student();
									s.setId(student_id+"");
									s.setRoll_no(roll_no);
									rollNoList.add(s);
								}
							}
						} else {
							Cursor cur1 = db.rawQuery(
									"Select * from subject_rollno where subject_id="
											+ subject_id + ";", null);
							if (cur1 != null) {
								rollNoList.clear();
								while (cur1.moveToNext()) {
									String roll_no=cur1.getString(1);
									Integer student_id=cur1.getInt(2);
									Student s=new Student();
									s.setId(student_id+"");
									s.setRoll_no(roll_no);
									rollNoList.add(s);
								}
							}
						}
					}
				} else {

				}
				i.putExtra("method", method);
				i.putExtra("rollNoList", rollNoList);
				NewAttendanceActivity.this.startActivity(i);
				//finish();
			}
		}
	};
	OnItemSelectedListener classListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String className = (String) parent.getItemAtPosition(position);
			if (className.equalsIgnoreCase("Select Class")) {
				class_id = -1;
			} else {
				helper = new DataHelper(c);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from classes where name='"
						+ className + "';", null);
				Log.e("Class", className);
				if (cur != null) {
					while (cur.moveToNext()) {
						class_id = cur.getInt(0);
						classCapacity = cur.getInt(2);
					}
					cur = db.rawQuery("Select * from subjects where class_id="
							+ class_id + ";", null);
					if (cur != null) {
						subjectList.clear();
						subjectList.add("Select Subject");
						while (cur.moveToNext()) {
							subjectList.add(cur.getString(1));
							subjectAdapter.notifyDataSetChanged();
						}
					}
					subjectName.setSelection(0);
					subjectName.setVisibility(View.VISIBLE);
				}
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};
	OnItemSelectedListener subjectListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String subjectName = (String) parent.getItemAtPosition(position);
			if (subjectName.equalsIgnoreCase("Select Subject")) {
				subject_id = -1;
			} else {
				helper = new DataHelper(c);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from subjects where name='"
						+ subjectName + "';", null);
				if (cur != null) {
					while (cur.moveToNext()) {
						subject_id = cur.getInt(0);
						/*
						 * String isElective=cur.getString(3);
						 * if(isElective.equals("1")){ Cursor cur1=db.rawQuery(
						 * "Select * from subject_rollno where subject_id="
						 * +subject_id+";", null); if(cur1!=null){
						 * rollNoList.clear(); while(cur1.moveToNext()){
						 * rollNoList.add(Integer.valueOf(cur1.getString(1))); }
						 * } } else{ classCapacity=cur.getInt(4); int i=1;
						 * rollNoList.clear(); while(i<=classCapacity){
						 * rollNoList.add(i); i++; } }
						 */

					}
					cur = db.rawQuery("Select * from topics where subject_id="
							+ subject_id + ";", null);
					if (cur != null) {
						topicList.clear();
						topicList.add("Select Topic");
						while (cur.moveToNext()) {
							topicList.add(cur.getString(1));
							topicAdapter.notifyDataSetChanged();
						}
					}
					topicName.setSelection(0);
					topicName.setVisibility(View.VISIBLE);
				}
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};
	OnItemSelectedListener topicListener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String topicName = (String) parent.getItemAtPosition(position);
			if (topicName.equalsIgnoreCase("Select Topic")) {
				topic_id = -1;
			} else {
				helper = new DataHelper(c);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from topics where name='"
						+ topicName + "';", null);
				if (cur != null) {
					while (cur.moveToNext()) {
						topic_id = cur.getInt(0);
					}
					cur = db.rawQuery("Select * from slots;", null);
					if (cur != null) {
						slotList.clear();
						slotList.add("Select Slot");
						while (cur.moveToNext()) {
							slotList.add(cur.getString(1) + "-"
									+ cur.getString(2));
							slotAdapter.notifyDataSetChanged();
						}
					}
					slotDetails.setSelection(0);
					slotDetails.setVisibility(View.VISIBLE);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}

	};
	OnItemSelectedListener slotListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String slotName = (String) parent.getItemAtPosition(position);
			if (slotName.equalsIgnoreCase("Select Slot")) {
				slot_id = -1;
			} else {

				helper = new DataHelper(c);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from slots where name='" + slotName
						+ "';", null);
				if (cur != null) {
					while (cur.moveToNext()) {
						slot_id = cur.getInt(0);
					}
					cur = db.rawQuery("Select * from attendancetype;", null);
					if (cur != null) {
						typeList.clear();
						typeList.add("Select Type");
						while (cur.moveToNext()) {
							typeList.add(cur.getString(1));
							typeAdapter.notifyDataSetChanged();
						}
					}
					attendanceType.setSelection(0);
					attendanceType.setVisibility(View.VISIBLE);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};
	OnItemSelectedListener typeListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String typeName = (String) parent.getItemAtPosition(position);
			if (typeName.equalsIgnoreCase("Select Type")) {
				type_id = -1;
			} else {

				helper = new DataHelper(c);
				db = helper.getReadableDatabase();
				cur = db.rawQuery("Select * from attendancetype where type='"
						+ typeName + "';", null);
				if (cur != null) {
					while (cur.moveToNext()) {
						type_id = cur.getInt(0);
					}
				}
				attendanceMethod.setVisibility(View.VISIBLE);
				attendanceMethod.setSelection(0);
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};
	OnItemSelectedListener methodListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			method= (String) parent.getItemAtPosition(position);
			if(!method.equals("Select Marking Method")){
				confirmDetails.setVisibility(View.VISIBLE);
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		MenuItems items = new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
