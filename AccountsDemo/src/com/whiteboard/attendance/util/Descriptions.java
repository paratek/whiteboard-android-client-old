package com.whiteboard.attendance.util;

public class Descriptions {
	public static String LOGIN_REQUEST="login";
	public static String PUSH_ATTENDANCE="";
	public static String GET_STUDENT_RECORD="";
	public static String GET_FACULTY_CLASS="";
	public static String GET_FACULTY_CLASS_STUDENTS="";
	public static String GET_CLASS_STUDENT_ATTENDANCE="";
	public static String GET_CLASS_STUDENT_DETAIL="";
}
