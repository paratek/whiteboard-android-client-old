package com.whiteboard.attendance.util;

public class JsonValues {
	//Response
	public static String teacher_name="";
	//pushattendance
	public static String date="date";
	public static String timestamp="timestamp";
	public static String present_array="present";
	public static String absent_array="absent";
	public static String attendance_type="is_lab";
	public static String subject_id="subject_id";
	public static String module_id="module_id";
	public static String data="data";
	public static String timeslot_id="timeslot_id";
	//token
	public static String token="token";
	public static String token_type="token_type";
	public static String expires_in="expires_in";
	//content-type
	public static String content_type="content-type";
	//user
	public static String user="user";
	public static String email="email";
	public static String name="name";
	//timeslots
	public static String slot_array="timeslots";
	public static String slot_name="name";
	public static String start_time="start_time";
	public static String end_time="end_time";
	public static String slot_id="id";	
	//classes
	public static String class_array="classes";
	public static String class_name="name";
	public static String department_name="dept";
	public static String class_capacity="capacity";
	public static String id="id";
	//subjects
	public static String subject_array="subjects";
	public static String subject_name="name";
	public static String subject_elective="is_elective";
	//rollNos
	public static String rollNo_array="students";
	public static String roll_no="roll_no";
	//public static String
	//topics
	public static String topic_array="modules";
	public static String topic_name="description";
	
	
	//Request
	public static String access_token="access_token";
	public static String grant_type="grant_type";
	public static String description="description";
	public static String route="route";
	public static String client_id="client_id";
	public static String client_secret="client_secret";
	public static String username="username";
	public static String password="password";
	//Values
	public static String grant_type_value="";
	
}
