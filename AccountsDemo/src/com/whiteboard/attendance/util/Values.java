package com.whiteboard.attendance.util;

import java.util.ArrayList;

public class Values {

	public static String username = "";
	public static String client_id = "f3d259ddd3ed8ff3843839b";
	public static String client_secret = "4c7f6f8fa93d59c45502c0ae8c4a95b";
	public static String access_token = "";
	public static Teacher teacher = new Teacher();
	public static ArrayList<Student> presentNoList = new ArrayList<Student>();
	public static ArrayList<Student> absentNoList = new ArrayList<Student>();
	// Shared Preferences String Values
	public static String USER_INFO = "USER_INFO";
	public static String USER_NAME = "USER_NAME";
	public static String CLIENT_ID = "CLIENT_ID";
	public static String CLIENT_SECRET = "CLIENT_SECRET";
	public static String ACCESS_TOKEN = "ACCESS_TOKEN";
	public static String TEACHER_NAME = "TEACHER_NAME";
	public static String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		Values.username = username;
	}

	public static String getClient_id() {
		return client_id;
	}

	public static void setClient_id(String client_id) {
		Values.client_id = client_id;
	}

	public static String getClient_secret() {
		return client_secret;
	}

	public static void setClient_secret(String client_secret) {
		Values.client_secret = client_secret;
	}

	public static String getAccess_token() {
		return access_token;
	}

	public static void setAccess_token(String access_token) {
		Values.access_token = access_token;
	}

	public static Teacher getTeacher() {
		return teacher;
	}

	public static void setTeacher(Teacher teacher) {
		Values.teacher = teacher;
	}

	public ArrayList<Student> getPresentNoList() {
		return presentNoList;
	}

	public void setPresentNoList(ArrayList<Student> presentNoList) {
		this.presentNoList = presentNoList;
	}

	public ArrayList<Student> getAbsentNoList() {
		return absentNoList;
	}

	public void setAbsentNoList(ArrayList<Student> absentNoList) {
		this.absentNoList = absentNoList;
	}

}
