package com.whiteboard.attendance.util;

public class URLs {
	public static String BASE_URL="https://whiteboard-1.vagrantshare.com";
	public static String LOGIN_REQUEST="/oauth/access_token";
	public static String PUSH_ATTENDANCE="/api/attendance";
	public static String GET_STUDENT_RECORD="";
	public static String GET_FACULTY_CLASS="";
	public static String GET_FACULTY_CLASS_STUDENTS="";
	public static String GET_CLASS_STUDENT_ATTENDANCE="";
	public static String GET_CLASS_STUDENT_DETAIL="";
}
