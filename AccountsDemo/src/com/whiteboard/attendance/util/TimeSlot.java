package com.whiteboard.attendance.util;

public class TimeSlot {
	String end_time;
	String start_time;
	String slot_id;
	public TimeSlot(){
		
	}
	public TimeSlot(String end_time, String start_time, String slot_name) {
		super();
		this.end_time = end_time;
		this.start_time = start_time;
		this.slot_id = slot_name;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getSlot_id() {
		return slot_id;
	}
	public void setSlot_id(String slot_id) {
		this.slot_id = slot_id;
	}
	
	
}
