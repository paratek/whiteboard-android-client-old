package com.whiteboard.attendance.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelper extends SQLiteOpenHelper {
	public static final String dbname="whiteboarddb";
	public static final int dbversion=1;
	private Context context;
	public DataHelper(Context context){
		super(context,dbname,null , dbversion);
		this.setContext(context);
		// TODO Auto-generated constructor stub
	}
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("Create table if not exists slots(id INTEGER PRIMARY KEY AUTOINCREMENT,start_time TEXT,end_time TEXT,name TEXT);");//TimeSlots
		db.execSQL("Create table if not exists attendancetype(id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT);");//TimeSlots
		//db.execSQL("Create table if not exists teacher(id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR(200),dept VARCHAR(200));");
		db.execSQL("Create table if not exists teacher(id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR(200));");
		db.execSQL("Create table if not exists classes(id INTEGER PRIMARY KEY ,name VARCHAR(200),capacity INTEGER,dept VARCHAR(200));");
		db.execSQL("Create table if not exists subjects(id INTEGER PRIMARY KEY ,name VARCHAR(200),class_id INTEGER, is_elective TEXT,classCapacity INTEGER,"
				+ "FOREIGN KEY(class_id) REFERENCES classes(id)) ;");
		db.execSQL("Create table if not exists class_rollno(class_id INTEGER,roll_no TEXT,student_id INTEGER,"
				+ "FOREIGN KEY(class_id) REFERENCES class(id));");
		db.execSQL("Create table if not exists subject_rollno(subject_id INTEGER,roll_no TEXT,student_id INTEGER,"
				+ "FOREIGN KEY(subject_id) REFERENCES subjects(id));");
		db.execSQL("Create table if not exists topics(id INTEGER PRIMARY KEY ,name VARCHAR(200),subject_id INTEGER,"
				+ "FOREIGN KEY(subject_id) REFERENCES subjects(id)) ;");
		db.execSQL("Create table if not exists pendingattendance(id INTEGER PRIMARY KEY AUTOINCREMENT,class_id INTEGER,subject_id INTEGER,topic_id INTEGER,slot_id INTEGER, type_id INTEGER ,"
				+ "date TEXT,timestamp TEXT,FOREIGN KEY(type_id) REFERENCES attendancetype(id),"
				+ "FOREIGN KEY(subject_id) REFERENCES subjects(id),"+ "FOREIGN KEY(class_id) REFERENCES classes(id),"+"FOREIGN KEY(topic_id) REFERENCES topics(id),"
				+"FOREIGN KEY(slot_id) REFERENCES slots(id));");
		
		db.execSQL("Create table if not exists attendance(pendingattendance_id INTEGER ,rollno INTEGER,present TEXT,student_id INTEGER,"
				+"FOREIGN KEY(pendingattendance_id) REFERENCES pendingattendance(id));");
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
}
