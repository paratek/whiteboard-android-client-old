package com.whiteboard.attendance.helper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.ui.ClientDetailsActivity;
import com.whiteboard.attendance.ui.LoginActivity;
import com.whiteboard.attendance.ui.URLActivity;

public class MenuItems {
	private Context c;
	DataHelper helper;
	SQLiteDatabase db;
	public MenuItems(Context c) {
		super();
		this.c = c;
	}
	public void process(int id){
		if(id==R.id.editURL){
			Intent intent=new Intent(c,URLActivity.class);
			((Activity)c).startActivity(intent);
		}
		else if(id==R.id.clientDetails){
			Intent intent=new Intent(c,ClientDetailsActivity.class);
			((Activity)c).startActivity(intent);
		}
		else if(id==R.id.logout){
			AlertDialog.Builder builder2 = new AlertDialog.Builder(
					c);
			builder2.setTitle("Logout?");
			builder2.setMessage("Do you want to really logout?"
					);
			builder2.setNegativeButton(
					"No",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface dialog,
								int which) {
							dialog.dismiss();
						}
					});
			builder2.setPositiveButton(
					"Yes",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface dialog,
								int which) {
							// TODO Auto-generated method stub
							helper=new DataHelper(c);
							db=helper.getWritableDatabase();
							db.execSQL("Delete FROM slots;");
							db.execSQL("Delete FROM teacher;");
							db.execSQL("Delete FROM classes;");
							db.execSQL("Delete FROM subjects;");
							db.execSQL("Delete FROM class_rollno;");
							db.execSQL("Delete FROM subject_rollno;");
							db.execSQL("Delete FROM topics;");
							db.execSQL("Delete FROM pendingattendance;");
							db.execSQL("Delete FROM attendance;");
							db.execSQL("Delete FROM attendancetype;");
							AccountManager manager;
							manager = AccountManager.get(c);
							String accountType = "com.whiteboard";
							Account[] accounts=manager.getAccountsByType(accountType);
						
							manager.removeAccount(accounts[0], new AccountManagerCallback<Boolean>() {
								
								@Override
								public void run(AccountManagerFuture<Boolean> arg0) {
									// TODO Auto-generated method stub
									
								}
							}, new Handler());
							Intent intent=new Intent(c,LoginActivity.class);
							((Activity)c).startActivity(intent);
							((Activity)c).finish();
						}
					
					});
			builder2.show();
		}
	}
}
