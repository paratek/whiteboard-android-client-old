package com.whiteboard.attendance.helper;

import android.content.Context;
import android.widget.Toast;

public class MessageHelper {
	Context context;
	
	public MessageHelper(Context context) {
		super();
		this.context = context;
	}
	public void displayShort(String s){
		Toast.makeText(context,s, Toast.LENGTH_SHORT).show();
	}
	public void displayLong(String s){
		Toast.makeText(context,s, Toast.LENGTH_LONG).show();
	}
}
