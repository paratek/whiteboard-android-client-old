package com.whiteboard.attendance.adapters;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.util.Student;
import com.whiteboard.attendance.util.Values;

public class GridAdapter extends BaseAdapter {
	Context context;
	ArrayList<Student> rollNoList = new ArrayList<Student>();
	Boolean markAbsent;
	ArrayList<View> gridElements=new ArrayList<View>();
	
	public GridAdapter(Context context, ArrayList<Student> rollNoList,
			Boolean markAbsent) {
		super();
		this.context = context;
		this.rollNoList = rollNoList;
		this.markAbsent = markAbsent;
		//initializer();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rollNoList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		final Holder holder;
		LayoutInflater inflater;
		LinearLayout gridBackground = null;
		TextView rollNo;
		final Student student = rollNoList.get(position);
		if(row==null)
		{
		inflater = (LayoutInflater) ((Activity) context)
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = inflater.inflate(R.layout.grid_item, parent, false);
		holder = new Holder();
		row.setTag(holder);
		gridBackground = (LinearLayout) row.findViewById(R.id.gridBackground);
		rollNo = (TextView) row.findViewById(R.id.rollNo);
		holder.gridBackground = gridBackground;
		holder.rollNo = rollNo;
		}
		else{
			holder = (Holder) row.getTag();
		}
		Boolean flag=false;
		Iterator<Student> i=Values.presentNoList.iterator();
		while(i.hasNext()){
			if(i.next().getId().equals(student.getId())){
				flag=true;
			}
		}
		if(flag)
			holder.gridBackground.setBackgroundColor(Color.GREEN);
		else
			holder.gridBackground.setBackgroundColor(Color.RED);
		holder.rollNo.setText(student.getRoll_no());
		holder.gridBackground.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Boolean flag=false;
				String id=student.getId();
				Student s=new Student();
				s.setId(student.getId());
				s.setRoll_no(student.getRoll_no());
				int j=0;
				for(int i=0;i<Values.presentNoList.size();i++){
					String id1=Values.presentNoList.get(i).getId();
					if(id1.equals(id)){
						flag=true;
						j=i;
					}
				}
				if(flag){
					v.setBackgroundColor(Color.RED);
					Values.absentNoList.add(s);
					Values.presentNoList.remove(j);
					
				}else{
					j=0;
					flag=false;
					for(int i=0;i<Values.absentNoList.size();i++){
						String id1=Values.absentNoList.get(i).getId();
						if(id1.equals(id)){
							flag=true;
							j=i;
						}
					}
					if(flag){
						v.setBackgroundColor(Color.GREEN);
						Values.presentNoList.add(s);
						Values.absentNoList.remove(j);
					}
					else{
						
					}
				}
				TextView presentTextView = (TextView) ((Activity)context).findViewById(R.id.presentDetails);
				presentTextView.setText("Present: "+Values.presentNoList.size()+" / "+rollNoList.size());
				Log.e("ABSENT", Values.absentNoList.toString());
			}
		});
		return row;
	}

	static class Holder {
		private LinearLayout gridBackground;
		private TextView rollNo;
		private int id;
	}

}
